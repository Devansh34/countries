fetch("https://restcountries.com/v3.1/all")
  .then((data) => {
    return data.json();
  })
  .then((data) => {
    data.map((country) => {
      createCountryCard(country);
    });

    const search = document.querySelector('[name="search"]');
    search.addEventListener("keyup", searching);

    const filter = document.querySelector(".select");
    filter.addEventListener("change", () => filtering(data));

    const mode = document.querySelector(".mode");
    mode.addEventListener("click", () => themeMode());
  });

function searching(e) {
  const searchedWord = e.target.value.toLowerCase();
  const countries = document.querySelectorAll(".countryName");
  Array.from(countries).forEach((country) => {
    const count = country.firstChild.textContent;
    if (count.toLowerCase().indexOf(searchedWord) != -1) {
      country.parentElement.style.display = "block";
    } else {
      country.parentElement.style.display = "none";
    }
  });
}

function filtering(data) {
  const filterRegion = document.querySelector(".select").value;
  const countries = document.querySelectorAll(".region");
  Array.from(countries).forEach((country) => {
    country.parentElement.parentElement.remove();
  });
  data.forEach((country) => {
    if (filterRegion == "All") {
      createCountryCard(country);
    } else if (country.region == filterRegion) {
      createCountryCard(country);
    }
  });
}

function createCountryCard(country) {
  const countries = document.querySelector(".countryContainer");
  const countryCard = document.createElement("div");
  countryCard.className = "countryCard";
  countryCard.classList.add("white");
  countries.appendChild(countryCard);
  const flag = document.createElement("img");
  flag.setAttribute("src", country.flags.png);

  const ul = document.createElement("ul");
  ul.style.listStyle = "none";

  const countryName = document.createElement("p");
  countryName.className = "countryName";
  countryName.innerText = country.name.common;
  // countryName.style.fontWeight = "800";

  const li1 = document.createElement("li");
  const p1 = document.createElement("p");
  const title1 = document.createElement("span");
  title1.innerText = "Population: ";
  title1.className = "title";
  const value1 = document.createElement("span");
  value1.innerText = `${country.population.toLocaleString()}`;
  p1.append(title1, value1);
  li1.append(p1);

  const li2 = document.createElement("li");
  const p2 = document.createElement("p");
  const title2 = document.createElement("span");
  title2.innerText = "Region: ";
  title2.className = "title";
  const value2 = document.createElement("span");
  value2.innerText = `${country.region}`;
  p2.append(title2, value2);
  li2.append(p2);
  li2.className = "region";

  const li3 = document.createElement("li");
  const p3 = document.createElement("p");
  const title3 = document.createElement("span");
  title3.innerText = "Capital: ";
  title3.className = "title";
  const value3 = document.createElement("span");
  value3.innerText = `${country.capital}`;
  p3.append(title3, value3);
  li3.append(p3);

  ul.append(li1, li2, li3);
  countryCard.append(flag, countryName, ul);
  const text = document.querySelector(".mode");
  if (text.textContent !== "Dark Mode") {
    countryCard.classList.remove("white");
    countryCard.classList.add("darkBlue");
  } else {
    countryCard.classList.remove("darkBlue");
    countryCard.classList.add("white");
  }
}

function themeMode(e) {
  const text = document.querySelector(".mode");
  console.log(text.textContent);
  const nav = document.getElementsByClassName("nav")[0];
  const countries = document.getElementsByTagName("section")[0];
  const countryCard = document.querySelectorAll(".countryCard");
  const search = document.getElementsByClassName("search")[0];
  const input = document.getElementsByTagName("input")[0];
  const select = document.getElementsByClassName("select")[0];
  const body = document.getElementsByTagName("body")[0];
  const searchIcon = document.getElementsByClassName("fa-magnifying-glass")[0];
  const modeIcon=document.getElementsByClassName("fa-moon")[0]
  if (text.textContent === "Dark Mode") {
    text.textContent = "Light Mode";
    countries.classList.add("veryDarkBlue");
    nav.classList.add("darkBlue");
    countryCard.forEach((country) => {
      country.classList.remove("white");
    });
    countryCard.forEach((country) => {
      country.classList.add("darkBlue");
    });
    search.classList.add("darkBlue");
    search.classList.remove("white");
    select.classList.add("darkBlue");
    select.classList.remove("white");
    body.classList.add("veryDarkBlue");
    input.classList.add("darkBlue");
    input.classList.remove("white");
    searchIcon.classList.add("darkBlue")
    modeIcon.classList.remove("fa-regular")
    modeIcon.classList.add("fa-solid")

  } else {
    text.textContent = "Dark Mode";
    countries.classList.add("white");
    countries.classList.remove("veryDarkBlue");
    nav.classList.remove("darkBlue");
    nav.classList.add("white");
    countryCard.forEach((country) => {
      country.classList.remove("darkBlue");
    });
    countryCard.forEach((country) => {
      country.classList.add("white");
    });
    search.classList.remove("darkBlue");
    search.classList.add("white");
    select.classList.remove("darkBlue");
    select.classList.add("white");
    body.classList.remove("veryDarkBlue");
    input.classList.remove("darkBlue");
    input.classList.add("white");
    searchIcon.classList.remove("darkBlue")
    modeIcon.classList.add("fa-regular")
    modeIcon.classList.remove("fa-solid")
  }
}
